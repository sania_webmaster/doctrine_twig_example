<?php
	class Request
	{
		
		public $post = array();
		public $get = array();
		public $server = array();
		public $session = array();
		public $cookie = array();
        public $files = array();
        
		function __construct(){
			foreach($_SERVER as $key=>$val){
				$this->server[$key] = $val;
			}
			
			foreach($_GET as $key=>$val){
				$this->get[$key] = $val;
			}
			
			foreach($_POST as $key=>$val){
				$this->post[$key] = $val;
			}
			
			if (isset($_SESSION))
                foreach($_SESSION as $key=>$val){
                    $this->session[$key] = $val;
                }
            
            if (isset($_COOKIE))
                foreach($_COOKIE as $key=>$val){
                    $this->cookie[$key] = $val;
                }
            
            if (isset($_FILES))
                foreach($_FILES as $key=>$val){
                    $this->files[$key] = $val;
                }
			
			
		}
  
		public function getUri(){
			$uri = $_SERVER['REQUEST_URI'];
			if (strpos($uri,'?') !== false){
				$uri = substr($uri,0,strpos($uri,'?'));
			}
			return $uri;
		}
	}