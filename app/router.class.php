<?php
	class Router{
	
		private $route_list;
		private $default_action = 'p404';
		
		function __construct( Array $route_list){
			$this->route_list = $route_list;
		}
		
		public function route(Request $req){
			$uri = $req->getUri();
			$action = isset($this->route_list[$uri])? $this->route_list[$uri] : $this->default_action;
			return $action;
		
		}
	
	}