<?php
	class App
	{
		private $entityManager;
		private $twig;
		private $request;
		private $router;
	
		function __construct($entityManager,Twig_Environment $twig, Router $router){
			$this->entityManager = $entityManager;
			$this->twig = $twig;
			$this->request = new Request();
			$this->router = $router;
		}
		
		public function getEntityManager(){
			return $this->entityManager;
		}
		
		public function getTwig(){
			return $this->twig;
		}
		
		public function getRequest(){
			return $this->request;
		}
		
		public function start(){
			$action = $this->router->route($this->request);
			$action($this,$this->request);
		}
  
  
	}	