<?php

/* index.html */
class __TwigTemplate_bdc8a2d9bbc8746ccdda6d6a9dd298093ecd736db22efffc370c006d77e64b04 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html/>
<html>
<head>
<title>Домашнее задание</title>
<meta charset=\"utf-8\"/>
</head>
<body>
<style>
.phpinfo
{
\twidth: 800px;
\theight: 400px;
\tborder: 2px solid red;
\toverflow: scroll;
\tmargin: 10px auto 0 auto;
}

.myinfo
{
\tmargin: 20px auto 20px auto;
\twidth: 300px;
}

.myinfo table,.myinfo td
{
\tfont-size: 16px;
\tpadding: 5px;
\tborder: 2px solid #00f;
}

.myinfo th
{
\tfont-size: 16px;
\tpadding: 5px;
\tborder: 2px solid #00f;
\tbackground-color: #ff0;
}

.myinfo tr:hover
{
\tfont-size: 16px;
\tpadding: 5px;
\tborder: 2px solid #00f;
\tbackground-color: #555;
}

.time
{
\twidth: 300px;
\tborder: 2px solid #f00;
\tfont-size: 20px;
\tcolor: #f70;
\tmargin: 20px auto 0 auto;
\tpadding: 5px;
\ttext-align: center;
}

</style>

<div class=\"myinfo\">
<h3>Личные данные</h3>
<table>
<th>Имя</th>
<th>Фамилия</th>
<th>Возраст</th>
<th>Оклад</th>

";
        // line 68
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["userData"]) ? $context["userData"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 69
            echo "<tr>
\t<td><a title=\"Удалить\" href=\"/del_user?id=";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "name", array()), "html", null, true);
            echo "</a></td>
\t<td>";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "lastname", array()), "html", null, true);
            echo "</td>
\t<td>";
            // line 72
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "age", array()), "html", null, true);
            echo "</td>
\t<td>";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "salary", array()), "html", null, true);
            echo "</td>
\t
</tr>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 78
        echo "
</table>
</div>
<h3>Добавить сотрудника</h3>
<form method=\"post\" action=\"/add_user\">
\t<input type=\"text\" placeholder=\"name\" name=\"name\"/><br/>
\t<input type=\"text\" placeholder=\"lastname\" name=\"lastname\"/><br/>
\t<input type=\"text\" placeholder=\"age\" name=\"age\"/><br/>
\t<input type=\"text\" placeholder=\"salary\" name=\"salary\"/>
\t<button>Ок</button>
</form>


<h3>Книги</h3>
<table>
<th>Название</th>
<th>Автор</th>
<th>Выдана</th>

";
        // line 97
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["books"]) ? $context["books"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["book"]) {
            // line 98
            echo "<tr>
\t<td><a title=\"Удалить\" href=\"/del_book?id=";
            // line 99
            echo twig_escape_filter($this->env, $this->getAttribute($context["book"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["book"], "title", array()), "html", null, true);
            echo "</a></td>
\t<td>";
            // line 100
            echo twig_escape_filter($this->env, $this->getAttribute($context["book"], "author", array()), "html", null, true);
            echo "</td>
\t<td>";
            // line 101
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["book"], "userId", array()), "name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["book"], "userId", array()), "lastname", array()), "html", null, true);
            echo "</td>
\t
</tr>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['book'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 106
        echo "
</table>


<div id=\"time\">0000/00/00 00:00:00</div>

</body>

<script type=\"text/javascript\" src=\"js/jquery-1.11.0.min.js\"></script>
<script type=\"text/javascript\">
function getTime()
{
\t\$('#time').load(\"/gettime\");
}

setInterval( getTime, 1000 );
</script>

</html>



";
    }

    public function getTemplateName()
    {
        return "index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  171 => 106,  158 => 101,  154 => 100,  148 => 99,  145 => 98,  141 => 97,  120 => 78,  109 => 73,  105 => 72,  101 => 71,  95 => 70,  92 => 69,  88 => 68,  19 => 1,);
    }
}
/* <!DOCTYPE html/>*/
/* <html>*/
/* <head>*/
/* <title>Домашнее задание</title>*/
/* <meta charset="utf-8"/>*/
/* </head>*/
/* <body>*/
/* <style>*/
/* .phpinfo*/
/* {*/
/* 	width: 800px;*/
/* 	height: 400px;*/
/* 	border: 2px solid red;*/
/* 	overflow: scroll;*/
/* 	margin: 10px auto 0 auto;*/
/* }*/
/* */
/* .myinfo*/
/* {*/
/* 	margin: 20px auto 20px auto;*/
/* 	width: 300px;*/
/* }*/
/* */
/* .myinfo table,.myinfo td*/
/* {*/
/* 	font-size: 16px;*/
/* 	padding: 5px;*/
/* 	border: 2px solid #00f;*/
/* }*/
/* */
/* .myinfo th*/
/* {*/
/* 	font-size: 16px;*/
/* 	padding: 5px;*/
/* 	border: 2px solid #00f;*/
/* 	background-color: #ff0;*/
/* }*/
/* */
/* .myinfo tr:hover*/
/* {*/
/* 	font-size: 16px;*/
/* 	padding: 5px;*/
/* 	border: 2px solid #00f;*/
/* 	background-color: #555;*/
/* }*/
/* */
/* .time*/
/* {*/
/* 	width: 300px;*/
/* 	border: 2px solid #f00;*/
/* 	font-size: 20px;*/
/* 	color: #f70;*/
/* 	margin: 20px auto 0 auto;*/
/* 	padding: 5px;*/
/* 	text-align: center;*/
/* }*/
/* */
/* </style>*/
/* */
/* <div class="myinfo">*/
/* <h3>Личные данные</h3>*/
/* <table>*/
/* <th>Имя</th>*/
/* <th>Фамилия</th>*/
/* <th>Возраст</th>*/
/* <th>Оклад</th>*/
/* */
/* {% for user in userData %}*/
/* <tr>*/
/* 	<td><a title="Удалить" href="/del_user?id={{user.id}}">{{user.name}}</a></td>*/
/* 	<td>{{user.lastname}}</td>*/
/* 	<td>{{user.age}}</td>*/
/* 	<td>{{user.salary}}</td>*/
/* 	*/
/* </tr>*/
/* */
/* {% endfor %}*/
/* */
/* </table>*/
/* </div>*/
/* <h3>Добавить сотрудника</h3>*/
/* <form method="post" action="/add_user">*/
/* 	<input type="text" placeholder="name" name="name"/><br/>*/
/* 	<input type="text" placeholder="lastname" name="lastname"/><br/>*/
/* 	<input type="text" placeholder="age" name="age"/><br/>*/
/* 	<input type="text" placeholder="salary" name="salary"/>*/
/* 	<button>Ок</button>*/
/* </form>*/
/* */
/* */
/* <h3>Книги</h3>*/
/* <table>*/
/* <th>Название</th>*/
/* <th>Автор</th>*/
/* <th>Выдана</th>*/
/* */
/* {% for book in books %}*/
/* <tr>*/
/* 	<td><a title="Удалить" href="/del_book?id={{book.id}}">{{book.title}}</a></td>*/
/* 	<td>{{book.author}}</td>*/
/* 	<td>{{book.userId.name}} {{book.userId.lastname}}</td>*/
/* 	*/
/* </tr>*/
/* */
/* {% endfor %}*/
/* */
/* </table>*/
/* */
/* */
/* <div id="time">0000/00/00 00:00:00</div>*/
/* */
/* </body>*/
/* */
/* <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>*/
/* <script type="text/javascript">*/
/* function getTime()*/
/* {*/
/* 	$('#time').load("/gettime");*/
/* }*/
/* */
/* setInterval( getTime, 1000 );*/
/* </script>*/
/* */
/* </html>*/
/* */
/* */
/* */
/* */
