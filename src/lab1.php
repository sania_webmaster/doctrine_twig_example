 <hr />
  <h3>����� �������� ����������</h3>

<?php

    $a = 7;
    $b = 111;
    
    echo "a = $a;   b = $b<br>" ;
    
    $a = $a ^ $b;
    $b = $a ^ $b;
    $a = $a ^ $b;
    
    echo "a = $a;   b = $b" ;
    
?>
<hr />
<h3>�������� ������a� ������</h3>

<?php

    /**
     *  function backPolishNotation Compute value expression 
     *  representation Back Polish Notation
     * 
     *  @param string $str Input string containes operand and 
     *                     operatrors separated backspase
     *  @return float $stack[$stackPointer] Computed value of input
     *                expression
     */
    function backPolishNotation( $str )
    {
        $items = explode( ' ', $str );
        $stack = array();
        $stackPointer = -1;
        
        foreach( $items as $item )
        {
            //echo $item;
            switch ( $item )
            {
                case "+": 
                    if ( $stackPointer > 0 )
                    {
                        $stack[ $stackPointer - 1 ] = $stack[ $stackPointer - 1 ] + $stack[ $stackPointer ]; 
                        $stackPointer--; 
                    } 
                    break; 
                   
                case "-": 
                    if ( $stackPointer > 0 )
                    {
                        $stack[ $stackPointer - 1 ] = $stack[ $stackPointer - 1 ] - $stack[ $stackPointer ];
                        $stackPointer--; 
                    }
                    break;
                case "*": 
                    if ( $stackPointer > 0 )
                    {
                        $stack[ $stackPointer - 1 ] = $stack[ $stackPointer - 1 ] * $stack[ $stackPointer ];
                        $stackPointer--;
                    }
                    break;
                case "/": 
                    if ( $stackPointer > 0 )
                    {
                        $stack[ $stackPointer - 1 ] = $stack[ $stackPointer - 1 ] / $stack[ $stackPointer ];
                        $stackPointer--;  
                    }
                    break;
                default:  if( ! empty( $item ) && is_numeric( $item ) )
                {
                    $stackPointer++;
                    $stack[ $stackPointer ] = floatval( $item );
                }
            }
        } 
        return( $stack[ $stackPointer ] );
       
    }//end func
    
    $examples = array
                    (
                        '5 8 3 + *',
                        '6 8 *',
                        '9 3 /',
                        '5 3 - 3 *',
                        ' 5   5ew 5 + 3 * + +'
                    );
    
    foreach( $examples as $string )
    {
        echo $string . ' = ' . backPolishNotation( $string ) . '<br>';
    }
    
?>
<hr />
<h3>������</h3>
<?php
        
    
    /**
     *  function move Move on square and fill array simbols of string
     *  @param int $row Row of square
     *  @param int $col Column of square
     *  @param string $curse Direction to move
     *  @param array $square Array representing cells of square
     *  @param int $n Square width
     *  @return bool true if move or false if change direction 
     */
    function move( &$row, &$col, &$curse, $square, $n ) //�������� �� �������� �� 1 ������ 
    {
        
        $changeCurse = array(
                                "r" => "d",
                                "d" => "l",
                                "l" => "u",
                                "u" => "r" 
                            );
                        
        switch( $curse )
        {
            case "l":   if ( $col > 1 && isCellEmpty( $square, $row, $col - 1 ) ) 
                        {
                            $col--; 
                            return true;
                        }  
                        else 
                        {
                            $curse = $changeCurse[ $curse ];
                            return false;
                        }
                         
            case "d":   if ( $row <= $n  && isCellEmpty( $square, $row + 1, $col ) )
                        {
                            $row++;
                            return true;
                        }
                        else
                        {
                            $curse = $changeCurse[ $curse ];
                            return false;
                        }
                          
            case "r":   if ( $col <= 5  && isCellEmpty( $square, $row, $col + 1 ) )
                        {
                            $col++;
                            return true;
                        }
                        else
                        {
                            $curse = $changeCurse[ $curse ];
                            return false;
                        }
            
            case "u":   if ( $row > 1  && isCellEmpty( $square, $row - 1, $col ) )
                        {
                            $row--;
                            return true;
                        }
                        else 
                        {
                            $curse = $changeCurse[ $curse ];
                            return false;
                        }
        }  
    }//end func
    
    
    /**
     *  function isCellEmpty Check is cell of square empty
     *  @param int $row Row of square
     *  @param int $col Column of square
     *  @param array $square Array representing cells of square
     *  @return bool true if cell empty or false if cell not empty 
     */
    function isCellEmpty( $array, $row, $col ) //�������� ������ �� ������
    {
        return ( isset( $array[$row][$col] ) && $array[$row][$col] === 0 ) ? true : false; 
    }//end func  
    
    
    /**
     *  function createSquare Create array 
     *  representing cells of squareand fill it by zero
     *  @param int $squareWidth Width of square
     *  @return array $square Created array 
     */
    function createSquare( $squareWidth )
    {
        $square = array();
        for( $i = 1; $i <= $squareWidth; $i++ )
            for( $j = 1; $j <= $squareWidth; $j++ )
                 $square[$i][$j] = 0; 
        return $square;  
        
    }//end func
    
    
    /**
     *  function countEmptyCell Count empty cell of square
     *  @param int $squareWidth Width of square
     *  @param array $square Array representing cells of square
     *  @return int $count Number empty cell of square
     */
    function countEmptyCell( $square, $squareWidth )
    {
        $count = 0;
        for( $i = 1; $i <= $squareWidth; $i++ )
            for( $j = 1; $j <= $squareWidth; $j++ )
                 if ( $square[$i][$j] === 0 )
                 {
                    $count++;
                 } 
        return $count;
    }//end func
    
    
    /**
     *  function printSquare Output array as table
     *  @param array $square Array representing cells of square
     */
    function printSquare( $square )
    {
        //������� ������ � ���� �������
        echo '<table>';
        foreach( $square as $row )
        {
            echo '<tr>';
            foreach( $row as $cell )
            {
                echo '<td>';
                echo ( $cell === 0)? ' ' : $cell; 
                echo '</td>';
            }
            echo '</tr>';
        }  
        echo '</table>';
        
    }//end func
    
    
    /**
     *  function snake Output string as lines snake
     *  @param string $str Input string
     *  @param int squareWidth Width of square
     */
    function snake( $str, $squareWidth )
    {
    
        $square = createSquare( $squareWidth );         
        //�������������� ��������� ��������
        $col = 1; //�������
        $row = 1; //���
        $curse = 'r'; //����������� ��������
        
        $strLength = strlen( $str );
        $index = 0;
        while( ( $index < $strLength ) && ( countEmptyCell( $square, $squareWidth) > 0 ) )
        {
            $square[$row][$col] = $str{$index};
            if ( move( $row, $col, $curse, $square, $squareWidth ) )
            {
                $index++;
            }
        }
        
        printSquare( $square );
    }//end func
    
    snake( '��������-������', 5 );
    
?>
<hr />
<h3>�����</h3>

<?php
    
    /**
     *  function printArray Print  array defined profile of roof
     *  @param array $array Given array
     */
    function printArray( $array )
    {
        $arrayLength = count( $array );
        for( $i = 0; $i < $arrayLength; $i++ )
        {
            echo $array[$i];
            echo ( $i == $arrayLength - 1 )? '' : ' ,';
        }
    }//end func
    
     /**
     *  function createSection Create and initialization array
     *  representative cells of roof
     *  @param array $array Given array
     *  @return array $section Array representing cells of roof
     */       
    //������� � ��������� 2-������ ������ ���������� ���� �����
    function createSection( $array )
    {
        $arrayLength = count( $array );
        //������� ������������ �������� ������� 
        $maxArray = 0;
        foreach( $array as $item )
        {
            if ( $maxArray < $item ) $maxArray = $item;
        }
        
        $section = array();
        for( $i = 0; $i < $arrayLength; $i++ )
        {
            for( $j = 1; $j <= $maxArray; $j++ )
            {
                if ( $j <= $array[$i] ) $section[$j][$i] = 1;
                else  $section[$j][$i] = 0;
            }
        }
        $section = array_reverse( $section );
        return $section;
    
    }//end func
    
    
     /**
     *  function paintSection Output as table array
     *  representative cells of roof
     *  @section array $section Given array
     */
    function paintSection( $section )
    {
        //������� ������ section  ���������� ���� ����� � ���� �������
    echo '<table>';
    foreach( $section as $row )
    {
        echo '<tr>';
        foreach( $row as $cell )
        {
            $emptyCell = '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
            $noemptyCell = '<td style="border: 1px silid grey; background-color: grey;">&nbsp;&nbsp;&nbsp;&nbsp;</td>';
            echo ( $cell === 0 )? $emptyCell : $noemptyCell;
        }
        echo '</tr>';
    }  
    echo '</table>';
    }//end func
    
     //������� ������ section  ���������� ���� ����� � ���� �������
    
    /**
     *  function paintWaterSection Output as table array
     *  representative cells of roof with cell filled water
     *  @section array $section Given array
     */
    function paintWaterSection( $section )
    {
        echo '<table>';
        foreach( $section as $row )
        {
            echo '<tr>';
            
            foreach( $row as $cell )
            {
                $emptyCell = '<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                $noemptyCell = '<td style="border: 1px silid grey; background-color: grey;">&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                $water = '<td style="border: 1px silid blue; background-color: blue;">&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                switch( $cell )
                {
                    case 0: echo $emptyCell; break;
                    case 1: echo $noemptyCell; break;
                    case 2: echo $water; break;
                } 
            }
            
            echo '</tr>';
        }  
        echo '</table>';
    }//end func
    
    //����� �� � ������ ����
    /**
     *  function willWaters Check water in cells after rain
     *  @param array $section Array representing cells of roof
     *  @param int $row Row of Array
     *  @param int $col Column of Array
     *  @return bool true if water will or false if otherwise
     */ 
    function willWaters( $section, $row, $col )
    {
        if ( $section[$row][$col] == 1 ) return false;
        
        $condition = isWayLeft( $section, $row, $col ) || 
        isWayRight( $section, $row, $col );
        
        if ( ! $condition )
        {
            return true;
        }
        return false;
        
    }//end func
    
    
    /**
     *  function isWayLeft Check is left way to drain
     *  @param array $section Array representing cells of roof
     *  @param int $row Row of Array
     *  @param int $col Column of Array
     *  @return bool true if is left way to drain or false if otherwise
     */ 
    function isWayLeft( $section, $row, $col )
    {
        $arrayLength = count( $section[$row] );
        $maxArray = 0;
        foreach( $section[$row] as $item )
        {
            if ( $maxArray < $item ) $maxArray = $item;
        }
        $isWay = true;
        for( $i = 0; $i < $col; $i++ )
        {
            if ( $section[$row][$i] == 1 ) $isWay = false;
        }
        return $isWay;
        
    }//end func
    
    
    /**
     *  function isWayRight Check is right way to drain
     *  @param array $section Array representing cells of roof
     *  @param int $row Row of Array
     *  @param int $col Column of Array
     *  @return bool true if is right way to drain or false if otherwise
     */ 
    function isWayRight( $section, $row, $col )
    {
        $arrayLength = count( $section[$row] );
        $maxArray = 0;
        foreach( $section[$row] as $item )
        {
            if ( $maxArray < $item ) $maxArray = $item;
        }
        $isWay = true;
        for( $i = $col + 1 ; $i < $arrayLength; $i++ )
        {
            if ( $section[$row][$i] == 1 ) $isWay = false;
        }
        return $isWay;
    }//end func
    
    
    /**
     *  function countWater Count water in cells
     *  @param array $section Array representing cells of roof
     *  @return int $countWaterCell Number cells filled water
     */
    function countWater( &$section )
    {
        $countWaterCell = 0;
        foreach( $section as $i => $row)
            foreach( $row as $j => $cell )
            {
                if ( willWaters( $section, $i, $j ) )
                {
                    $section[$i][$j] = 2;
                    $countWaterCell++;
                }  
            }
            return $countWaterCell;
    }//end func
    
    //�������� ������
    //$profil = array( 2, 5, 1, 2, 3, 4, 7, 7, 6 );
    $profil = array( 3, 1, 2, 1, 1, 6, 3, 3, 4 );
    //������� ������
    echo 'Array: '; 
    printArray( $profil );
    
    //������� 2-������ ������ ���������� ������� �����
    $section = createSection( $profil );
    
    //������ �����
    paintSection( $section );
    
    //������� ���������� ����� � �����
    $waterCell = countWater( $section );
    
    
    //������ ����� � �����
    echo '<br/><br/>';
    paintWaterSection( $section );
    
    //������� ���������
    echo '���������� ����: ' . $waterCell;
    
     
?>

<hr />
<h3>Int2Str</h3>
<?php

    /**
     * function int2str Converting integer value to string with formatting
     * @param int $number Integer number
     * @param int $maxnumber Maximal allowed integer value
     * @throws BadArgumentException if the provided argument $number is not of type 'integer'.
     * @throws OutputRangeException if the provided argument $number out of range 0 - 1000000000.
     * @return string $str String that is the decimal representation of that number grouped by commas after every 3 digits | false if fail
     */
    function int2str( $number, $maxnumber = 1000000000 )
    {
        //check given argument $number
        if ( ! is_int( $number ) )
        {
            throw new Exception( 'BadArgumentException' );  
        }
        if ( ( $number < 0 ) || ( $number >= $maxnumber ) )
        {
            throw new Exception( 'OutputRangeException' );    
        }
        $str = '';
        $count = 0;
        $tempStr = strval( $number );
        $length = strlen( $tempStr );
        for( $i = $length - 1; $i >= 0; $i-- )
        {
            $str = $tempStr{$i} . $str;
            $count++;
            if ( ( $count % 3 ) == 0 && $count != $length ) $str = ',' . $str;
        }
        return $str;
    }//end func

    //Example Usage
    echo 'Examples:<br>';
    $examples = array
                    (
                        32436378,
                        -212121,
                        1344343444,
                        327643322,
                        "321,22121,16"
                    );
    
    foreach( $examples as $example )
        try
        {
            if ( $str = int2str( $example ) )
            echo '<br>' . $example . ' -> ' . $str;
        }
        catch ( Exception $e )
        {
            echo '<br/>' . $example . ' - Error: ' . $e->getMessage();
        }
        

?>
<hr />
<h3>Anagrams</h3>
<?php
    
    /**
     * function anagrams Search all anagrams for given word from given file
     * @param string $word Word for which anagrams search 
     * @param string $wordFile Filename words list
     * @throws BadArgumentException if the provided argument $word is not of type 'string'.
     * @throws ShortArgumentException if length of the provided argument $word less 2. 
     * @throws OpenFileException if fail open file. 
     * @return array $anagrams Array contains point  anagrams
     */
    function anagrams( $word, $wordFile )
    {
        //check given argument $word
        if ( ! is_string( $word ) ) 
        {
            throw new Exception( 'BadArgumentException' );
        }
        
        if ( strlen( $word ) <  2 ) 
        {
            throw new Exception( 'ShortArgumentException' );
        }
        
        //count liters in the word
        $length = strlen( $word );
        $liters = array();
        for ( $i = 0; $i < $length; $i++ )
        { 
            if ( array_key_exists( $word{$i}, $liters ) )
            {
                $liters[$word{$i}]++;
            }
            else
            {
                $liters[$word{$i}] = 1;
            }
        }
        $countLiters = count( $liters );
        $anagrams = array();
        
        //open file | generate Exception if fail
        if ( $file = @fopen( $wordFile, 'r' ) )
        {
            while ( ! feof($file) ) //read each word and count liters
            {
                $tmpWord = fgets( $file );
                $tmpLength = strlen( $tmpWord ) - 1;
                //contunue if words different length
                if ( $tmpLength != $length ) continue;
                
                $tmpLiters = array();
                for ( $i = 0; $i < $tmpLength; $i++ )
                { 
                    if ( array_key_exists( $tmpWord{$i}, $tmpLiters ) )
                    {
                        $tmpLiters[$tmpWord{$i}]++;
                    }
                    else
                    {
                        $tmpLiters[$tmpWord{$i}] = 1;
                    }
                }

                $condition1 = ( $countLiters == count( $tmpLiters ) );
                $condition2 = ( count( array_diff_assoc( $liters, $tmpLiters ) ) == 0 );
                $condition3 = ( count( array_diff_assoc( $tmpLiters, $liters ) ) == 0 );                
                if ( $condition1 && $condition2 && $condition3 )
                {
                    $anagrams[] = $tmpWord;
                }
            }
            fclose( $file );
            return $anagrams;
        }
        else
        {
            throw new Exception('OpenFileException');
        } 
    }//end func
    
    
    //Example Usage
    try
    {
        $word = 'horse';
        $filename = 'wl.txt';
        echo 'Anagrams for word "' . $word . '" from file "' . $filename . '":';
        $anag = anagrams( $word, $filename );
        
        foreach( $anag as $item )
        {
            echo '<br>' . $item;
        }
    }
    catch ( Exception $e )
    {
        echo 'Error: ' . $e->getMessage();
    }
    
?>

<hr />
    