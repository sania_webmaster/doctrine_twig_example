<?php
	require_once "common.inc.php";
	
	function index($app,$req){
		$cacher = new Cacher();
		$cacher->begin($req);
		$file = __FILE__;
		$em = $app->getEntityManager();
		$twig = $app->getTwig();
		$userRepo = $em->getRepository('User');
		$users = $userRepo->findAll();
		
		$bookRepo = $em->getRepository('Book');
		$books = $bookRepo->findAll();
		header("Content-Type: text/html; charset=utf8");
		echo $twig->render('index.html', array('userData' => $users, 'books'=>$books, 'file' => $file));
		$cacher->end();
	}
	
	function p404($app,$req){
		$twig = $app->getTwig();
		echo $twig->render('p404.html');
	}
	
	function gettime($app,$req){
		echo date('Y/m/d H:i:s', time());
	}
	
	function add_user($app,$req){
		$req = $app->getRequest();
		if ($req->server['REQUEST_METHOD'] == 'POST'){
			$name = $req->post['name'];
			$lastname = $req->post['lastname'];
			$age = intval($req->post['age']);
			$salary = intval($req->post['salary']);
			
			$user = new User();
			$user->setName($name);
			$user->setLastname($lastname);
			$user->setAge($age);
			$user->setSalary($salary);
			$em = $app->getEntityManager();
			$em->persist($user);
			$em->flush();
			$cacher = new Cacher();
			$cacher->clear("/");
			$cacher->clear("/index.php");
			header("Location: /");
		}else{
            header("Location: /");
		}
	}
	
	function del_user($app,$req){
		$req = $app->getRequest();
		if (isset($req->get['id'])){
			$id = intval($req->get['id']);
		}
		$em = $app->getEntityManager();
		$user = $em->find('User', $id);
		try{
			$em->remove($user);
			$em->flush();
			
		}catch(Exception $ex){
			header("Location: /");
		}
		$cacher = new Cacher();
        $cacher->clear("/");
        $cacher->clear("/index.php");
		header("Location: /");
	}
	
	function clear_cache($app,$req){
        $cacher = new Cacher();
        $cacher->clear_all();
	}