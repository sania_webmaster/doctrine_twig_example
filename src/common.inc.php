<?php
	require_once "../app/bootstrap.php";
	require_once "../app/twig.php";
	require_once "controller.php";
	require_once "../app/app.class.php";
	require_once "../app/request.class.php";
	require_once "../app/router.class.php";
	require_once "../app/route_list.php";
	require_once "../app/cacher.class.php";