<?php
// src/Book.php
/**
 * @Entity @Table(name="books")
 **/
class Book
{
     /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="string") **/
    protected $title;
	
	/** @Column(type="string") **/
    protected $author;
	
	/**
     * @ManyToOne(targetEntity="User", inversedBy="books")
     **/
	protected $userId;
	
	
    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }
	
	public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }
	
	public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($id)
    {
        $this->userId = $id;
    }
}