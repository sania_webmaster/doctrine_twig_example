<?php
/**
 * @Entity @Table(name="users")
 **/
class User
{
     /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="string") **/
    protected $name;
	
	/** @Column(type="string") **/
    protected $lastname;
	
	/** @Column(type="integer") **/
    protected $age;
	
	/** @Column(type="integer") **/
    protected $salary;
	
	/**
     * @OneToMany(targetEntity="Book", mappedBy="userId")
     * @var Book[]
     **/
    protected $books = null;
	

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
	
	public function getLastname()
    {
        return $this->lastname;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }
	
	public function getAge()
    {
        return $this->age;
    }

    public function setAge($age)
    {
        $this->age = $age;
    }
	
	public function getSalary()
    {
        return $this->salary;
    }

    public function setSalary($salary)
    {
        $this->salary = $salary;
    }
	
	public function getBooks()
    {
        return $this->books;
    }

    public function setBooks($book)
    {
        $this->books[] = $book;
    }
	
	
}